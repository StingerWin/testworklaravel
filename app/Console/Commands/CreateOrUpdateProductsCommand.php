<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;

class CreateOrUpdateProductsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:createOrUpdate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create or update products from file prods.json';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $path_file = public_path('prods.json');
        if (file_exists($path_file)) {
            $content_file = json_decode(file_get_contents($path_file));
            if (Product::createOrUpdateFromFile($content_file)){
                echo 'The command is executed.';
            } else {
                echo 'Something went wrong.';
            }
        } else {
            echo 'File no exists to - ' . $path_file;
        }
    }
}
