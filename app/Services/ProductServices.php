<?php


namespace App\Services;


use App\Models\Characteristic;

class ProductServices
{
    public function formatProduct($product)
    {
        $format_product = $product->toArray();
        if (isset($format_product['characteristics'])) unset($format_product['characteristics']);

        foreach ($product->characteristics as $characteristic)
        $format_product['characteristics'][$characteristic->key] = $characteristic->value;

        return $format_product;
    }


    public function filterFormatCharacteristics()
    {
        $characteristics = Characteristic::all();
        $format_characteristics = [];
        foreach ($characteristics as $characteristic)
            $format_characteristics[$characteristic->key][$characteristic->value] = $characteristic->value;

        return $format_characteristics;
    }
}
