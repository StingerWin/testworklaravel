<?php

namespace App\Http\Controllers;

use App\Models\Characteristic;
use App\Models\Product;
use App\Services\ProductServices;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    protected $productServices;

    public function __construct(ProductServices $productServices)
    {
        $this->productServices = $productServices;
    }

    public function index()
    {
        $products = Product::all();

        $format_products = [];
        foreach ($products as $product) {
            $format_products[] = $this->productServices->formatProduct($product);
        }
        $products = $format_products;

        $filter_characteristics = $this->productServices->filterFormatCharacteristics();

        return view('index', compact('products', 'filter_characteristics'));
    }

    public function update_products()
    {
        $path_file = public_path('prods.json');
        if (file_exists($path_file)) {
            $content_file = json_decode(file_get_contents($path_file));
            $products = Product::createOrUpdateAndDeleteFromFile($content_file);
            $format_products = [];
            foreach ($products as $product) {
                $format_products[] = $this->productServices->formatProduct($product);
            }
            $products = $format_products;
            return response(['products' => $products]);
        } else {
            return response(['error' => 'File no exists to - ' . $path_file], 401);
        }
    }

    public function filter_products()
    {
        if (\request()->has('filters')) {
            $filters = request()->filters;

            if ($filters['name']) {
                $name = $filters['name'];
                $products = Product::where('name', 'LIKE', "%{$name}%")->get();
            } else {
                $products = Product::all();
            }

            if ($filters['price']) {
                switch ($filters['price']) {
                    case ('low'):
                        $products = $products->sortBy('price');
                        break;
                    case ('high'):
                        $products = $products->sortByDesc('price');
                        break;
                }
            }

            if (!empty($filters['characteristics'])) {
                $characteristics = $filters['characteristics'];
                $products = $products->filter(function ($query) use ($characteristics) {
                    $characteristics_db = $query->characteristics->whereIn('value', $characteristics);
                    return $characteristics_db->contains('product_id', $query->id);
                });
            }

            $format_products = [];
            foreach ($products as $product) {
                $format_products[] = $this->productServices->formatProduct($product);
            }
            $products = $format_products;

            return response()->json(['products' => $products], 200);
        }

        return response()->json(['error' => 'Something went wrong!'], 401);
    }
}
