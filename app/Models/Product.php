<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['id', 'name', 'price'];

    public function characteristics()
    {
        return $this->hasMany(Characteristic::class);
    }

    public static function createOrUpdateFromFile($file_products)
    {
        $ids_no_delete = [];
        foreach ($file_products as $product) {
            $products = self::all();

            $ids_no_delete[$product->id] = $product->id;

            $data = [
                'id' => $product->id,
                'name' => $product->name,
                'price' => $product->price,
            ];

            if ($products->contains('id', $product->id)) {
                unset($data['id']);
                $product_db = $products->find($product->id);
                $product_db->update($data);
            } else {
                $product_db = Product::create($data);
            }


            if (isset($product->characteristics)) {
                foreach ($product->characteristics as $key => $value) {
                    if ($products->contains('id', $product->id)) {
                        if (!$product_db->characteristics->isEmpty()){
                            $id = $product_db->characteristics->pluck('id')->toArray();
                            Characteristic::destroy($id);
                        }
                    }

                    Characteristic::create([
                        'product_id' => $product_db->id,
                        'key' => $key,
                        'value' => $value,
                    ]);

                }
            }
        }

        return Product::whereIn('id', $ids_no_delete)->get();
    }

    public static function createOrUpdateAndDeleteFromFile($file_products)
    {
        $ids_no_delete = self::createOrUpdateFromFile($file_products)->pluck('id')->toArray();
        Product::whereNotIn('id', $ids_no_delete)->delete();

        return self::all();
    }

}
