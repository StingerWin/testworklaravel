@extends('layouts.app')

@section('title')
    List product
@endsection

@section('styles')

@endsection

@section('content')
    <div class="card my-5">

        <div class="card-header">
            <h3>List product</h3>
        </div>
        <div class="card-body">
            <products-component :products="{{ json_encode($products) }}" :filter_characteristics="{{ json_encode($filter_characteristics) }}"></products-component>
        </div>

    </div>

@endsection

@section('scripts')

@endsection
